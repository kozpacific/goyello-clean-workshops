# Sprawdź czy masz zainstalowane
1. Java w wersji 10
2. Git
3. Intellij IDEA Community Edition (najlepiej w najnowszej wersji)

# Przygotowanie projektu
1. Utwórz folder do którego ściągniesz projekt
2. Uruchom command line i przejdź do stworzonego przed chwilą folderu
3. Wykonaj komendę `git clone https://gitlab.com/kozpacific/goyello-clean-workshops.git`
4. Uruchom IntelliJ następnie zaimportuj projekt w następujący sposób `File` > `New` > `Project From existing sources` > wybierz folder ze ściągniętym projektem > `OK` 
5. Konfiguracja importowanego projektu: `Import project from external model` > `Gradle` > `Next` > Zaznaczone opcje `use auto-import` oraz `use default gradle wrapper` > `Finish`
4. Kliknij prawym przyciskiem myszy na metodę `main()`, a następnie `Run Main.main()`
5. Jeżeli wszystko jest ok w konsoli powinieneś zobaczyć komunikat `Hello! Please type a command:`. 
6. Wypróbuj aplikację wpisując następujące komendu:
 * `buy-item -Utestuser -PtestPassword123# -Idesk`
 * `buy-item -Utestuser -PtestPassword123# -Icarpet`
 * `show-me-summary -Utestuser -PtestPassword123#`
 * `exit`
 
W razie problemów z poprawnym zaimportowanie projektu można się ze mną skontaktować pisząc na adres mailowy: `maciej.koziara@goyello.com`.
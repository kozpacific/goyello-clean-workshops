package com.goyello.workshops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static class User {

        public String username;
        public String password;

    }

    public static class History {

        public String item;
        public Integer price;

    }

    public static ArrayList<User> usrs = new ArrayList<>();
    public static HashMap<String, Integer> stock = new HashMap<>();
    public static HashMap<String, Integer> prices = new HashMap<>();
    public static HashMap<String, ArrayList<History>> history = new HashMap<>();

    static {
        stock.put("desk", 2);
        stock.put("chair", 6);
        stock.put("carpet", 1);

        prices.put("desk", 60);
        prices.put("chair", 10);
        prices.put("carpet", 20);
    }

    public static void main(String[] args) {

        boolean run = true;
        System.out.println("Hello! Please type a command:");

        do {
            try {
                System.out.print("> ");
                Scanner scanner = new Scanner(System.in);
                String fullCmd = scanner.nextLine();
                String[] fullCmdSplit = fullCmd.split(" ");

                if (fullCmdSplit.length == 0) {
                    throw new RuntimeException("No command provided");
                }

                String cmd = fullCmdSplit[0];
                String username = null;
                String password = null;
                String item = null;

                switch (cmd) {
                    case "buy-item": {
                        if (fullCmdSplit.length != 4) {
                            throw new RuntimeException("Missing arguments. Use buy-item command like this 'buy-item -U<username> -P<password> -I<item>'");
                        }

                        for (int i = 1; i < 4; i++) {
                            String arg = fullCmdSplit[i];
                            if (arg.startsWith("-U")) {
                                username = arg.substring(2);
                            } else if (arg.startsWith("-P")) {
                                password = arg.substring(2);
                            } else if (arg.startsWith("-I")) {
                                item = arg.replace("-I", "");
                            } else {
                                throw new RuntimeException("Unknown argument " + arg + ". Use buy-item command like this 'buy-item -U<username> -P<password> -I<item>'");
                            }
                        }


                        User user = null;
                        for (int i = 0; i < usrs.size(); i++) {
                            User u = usrs.get(0);
                            if (u.username.equals(username)) {
                                if (u.password.equals(password)) user = u;
                                else throw new RuntimeException("Incorrect password");
                            }
                        }

                        if (user == null) {
                            if (password != null && !password.isEmpty() && password.length() > 8 && !password.equals(username) && password.matches(".*[0-9].*")) {
                                System.out.println("User does not exist. Registering new user.");
                                User user1 = new User();
                                user1.username = username;
                                user1.password = password;

                                usrs.add(user1);
                                user = user1;
                            } else {
                                throw new RuntimeException("User does not exists and password is not strong enough");
                            }

                        }

                        Integer cnt = stock.get(item);
                        if (cnt == 0) {
                            throw new RuntimeException("There is no item " + item + " in the stock. Sorry!");
                        }

                        stock.put(item, cnt - 1);
                        ArrayList<History> hstItems = history.get(user.username);
                        if (hstItems == null) {
                            hstItems = new ArrayList<>();
                        }

                        History history = new History();
                        history.item = item;
                        history.price = prices.get(item);
                        hstItems.add(history);
                        Main.history.put(username, hstItems);

                        int total = 0;
//                        for (int i = 0; i < hstItems.size(); i++) {
//                            String it = hstItems.get(i);
//                            Integer pr = prices.get(it);
//                            total += pr;
//                        }

                        for (int i = 0; i < hstItems.size(); i++) {
                            History hs = hstItems.get(i);
                            Integer pr = hs.price;
                            total += pr;
                        }

                        System.out.println("You bought totally " + hstItems.size() + " items for " + total + ".");


                        break;
                    }
                    case "exit":
                        run = false;
                        System.out.println("Bye!");
                        break;
                    case "show-me-summary": {
                        if (fullCmdSplit.length != 3) {
                            throw new RuntimeException("Missing arguments. Use show-me-summary command like this 'show-me-summary -U<username> -P<password>'");
                        }

                        for (int i = 1; i < 3; i++) {
                            String arg = fullCmdSplit[i];
                            if (arg.startsWith("-U")) {
                                username = arg.substring(2);
                            } else if (arg.startsWith("-P")) {
                                password = arg.substring(2);
                            } else {
                                throw new RuntimeException("Unknown argument " + arg + ". Use show-me-summary command like this 'show-me-summary -U<username> -P<password>'");
                            }
                        }


                        User user = null;
                        for (int i = 0; i < usrs.size(); i++) {
                            User u = usrs.get(0);
                            if (u.username.equals(username)) {
                                if (u.password.equals(password)) user = u;
                                else throw new RuntimeException("Incorrect password");
                            }
                        }

                        if (user == null) {
                            if (password != null && !password.isEmpty() && password.length() > 8 && !password.equals(username) && password.matches(".*[0-9].*")) {
                                System.out.println("User does not exist. Registering new user.");
                                User user1 = new User();
                                user1.username = username;
                                user1.password = password;

                                usrs.add(user1);
                                user = user1;
                            } else {
                                throw new RuntimeException("User does not exists and password is not strong enough");
                            }

                        }

                        ArrayList<History> usrhist = history.get(user.username);

                        if (usrhist == null || usrhist.size() == 0) {
                            System.out.println("You bought nothing yet");
                        } else {
                            System.out.println("You bought:");
                            int total = 0;
                            for (int i = 0; i < usrhist.size(); i++) {
                                System.out.println(usrhist.get(i).item + " - " + usrhist.get(i).price);
                                total = total + usrhist.get(i).price;
                            }
                            System.out.println("You spent: " + total);
                        }
                        break;
                    }
                    default:
                        throw new RuntimeException("Unknown command: " + cmd);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        } while (run);
    }
}
